#!/usr/bin/ruby
require 'facter'

g10k_bin = Facter::Util::Resolution.exec("which g10k")
if File.exists?("#{g10k_bin}")
  version = Facter::Core::Execution.execute("#{g10k_bin} -version | egrep -o '[0-9]+\\.[0-9]+\\.[0-9]+'", options = {})
  Facter.add("g10k_version") do
    setcode do
      "#{version}"
    end
  end
end
